/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.program.mensajes_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author utp
 */
public class Conexion {
    public Connection get_connection(){
        Connection conection = null;
    try{
        //Esto es por facilidad para desarrollar en entornos locales. Es importante que en el momento de produccion se deba usar el host, el puerto, nombre de base de datos, usuario y contraseña propios.
        conection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes_app","root","");
        if(conection != null){
            System.out.println("conexion exitosa");
        }
        
    }catch(SQLException e){
        System.out.println(e);
    }
    return conection; //Retorna el objeto de conexión a la base de datos
    }
   
}
